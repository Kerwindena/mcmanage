#!/bin/python3

import os
import sys
import configparser
import shutil

REPOABSPATH = os.path.dirname(os.path.realpath(__file__)) + os.sep
CONFIGPATH = '..' + os.sep +'config' + os.sep + 'settings.ini'

SETTINGS = configparser.ConfigParser()

def load_config():
  try:
    settings = True
    try:
      with open(os.path.realpath(REPOABSPATH + CONFIGPATH)) as fh:
        settings = True
    except IOError:
      settings = False
    if not settings:
      path = os.path.realpath(REPOABSPATH + CONFIGPATH)
      shutil.copy(path + '.exmpl', path)
    SETTINGS.read(os.path.realpath(REPOABSPATH + CONFIGPATH))
  except IOError:
    print('No Settings supplied. (Possibly wrong permissions set)')

class Manager(object):
  def __start(self):
    pass
  def __stop(self):
    pass
  def __status(self):
    pass
  def __manage(self):
    pass
  def __backup(self):
    pass
  def __usage(self):
    print('Usage: ' + sys.argv[0] + ' (start|stop|status|manage|backup)' )
  def main(self):
    if len(sys.argv) != 2:
      self.__usage()
    elif sys.argv[1].lower() == 'start':
      self.__start()
    elif sys.argv[1].lower() == 'stop':
      self.__stop()
    elif sys.argv[1].lower() == 'status':
      self.__status
    elif sys.argv[1].lower() == 'mamage':
      self.__manage()
    elif sys.argv[1].lower() == 'backup':
      self.__backup
    else:
      self.__usage()


load_config()


manager = Manager()
manager.main()
